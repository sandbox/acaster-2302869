<?php
/**
 * @file
 * The admin form for configuring the "setup country" module.
 */

/**
 * Returns form for changing configuration options.
 */
function setup_countrygetlist_form($form, &$form_state) {
  include_once DRUPAL_ROOT . '/includes/iso.inc';

  $form = array();
  $form['setup_countrygetlist_blacklist'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Blacklist of the country'),
    '#description' => t('Marked the country will be excluded.'),
    '#default_value' => array_filter(variable_get('setup_countrygetlist_blacklist', array())),
    '#options' => _country_get_predefined_list(),
    '#element_validate' => array('setup_countrygetlist_validate'),
  );

  $form['setup_countrygetlist_whitelist'] = array(
    '#type' => 'textarea',
    '#title' => t('Whitelist of the country'),
    '#description' => t('Adds list of countries. Example "BY|Belarus". Each country on a new line.'),
    '#default_value' => variable_get('setup_countrygetlist_whitelist'),
    '#element_validate' => array('setup_countrygetlist_whitelist_validate'),
  );

  return system_settings_form($form);
}

/**
 * Validate-function for element "whitelist" form "setup_countrygetlist_form".
 */
function setup_countrygetlist_whitelist_validate($element, &$form_state, $form) {
  if (!empty($form_state['values']['setup_countrygetlist_whitelist'])) {
    $form_state['values']['setup_countrygetlist_whitelist'] = strip_tags($form_state['values']['setup_countrygetlist_whitelist']);
  }
}

/**
 * Validate-function for element "blacklist" form "setup_countrygetlist_form".
 */
function setup_countrygetlist_validate($element, &$form_state, $form) {
  form_set_value($element, array_filter($element['#value']), $form_state);
}
